﻿using Microsoft.Xna.Framework.Graphics;
using SGame.Client;
using SharedLibrary;
using SharedLibrary.GameEngine;
using SharedLibrary.Networking;
using SharedLibrary.Networking.Sockets;
using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TiledSharp;

namespace SGame.WinForm
{
    public partial class Home : Form
    {
        #region Variables
        private ResponseConnectionPacket _responseFromServer;
        private Engine _engine;
        private GameForm _game;
        private RemoteEngine _remoteEngine;

        /// <summary>
        /// Keeps supernodes that has received from server after first connection
        /// </summary>
        private List<Supernode> _supernodes;

        /// <summary>
        /// variable, that hold port number in case, this client will be SN. Needed for RMI channel connection
        /// </summary>
        private int _port;

        /// <summary>
        /// current ip. Needed to initiate remote connection
        /// </summary>
        private string _ip;
        #endregion

        #region Properties
        public string LabelState
        {
            get { return labelState.Text; }
            set { labelState.Text = value; }
        }
        #endregion

        #region Constructors
        public Home()
        {

            //generates random port number
            Random rnd = new Random();
            _port = rnd.Next(17001, 32000);
            _ip = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork)[0].ToString();
            InitializeComponent();
        }
        #endregion

        private void buttonJoin_Click(object sender, EventArgs e)
        {
            if (listBoxLobby.SelectedItem != null)
            {
                var selectedIMap = listBoxLobby.SelectedItem.ToString();
                var engineIp = _responseFromServer.Supernodes.FirstOrDefault(x => x.Map == selectedIMap).Ip;
                var enginePort = _responseFromServer.Supernodes.FirstOrDefault(x => x.Map == selectedIMap).Port;

                //var provider = new BinaryServerFormatterSinkProvider();
                //provider.TypeFilterLevel = TypeFilterLevel.Full;
                //var dictionary = new System.Collections.Hashtable();

                //// variable port needs to create tcp channel. it is completely different variable than _post. 
                //Random rnd = new Random();
                //var port = rnd.Next(2000, 62000);
                //dictionary["port"] = port;
                //dictionary["name"] = $"tcp{port}";
                //var clientRemotingChannel = new TcpChannel(dictionary, null, provider);
                //ChannelServices.RegisterChannel(clientRemotingChannel, false);
                //string fullUrl = $"tcp://{engineIp}:{enginePort}/remoteEngine";
                //_remoteEngine = (RemoteEngine)Activator.GetObject(typeof(RemoteEngine), fullUrl);

                //_game.CurrentEngine = _remoteEngine;
                GameData.NewEngineData = $"{engineIp}:{enginePort}";
                _game.Run();

                //}
            }
        }

        private void buttonConnectToServer_Click(object sender, EventArgs e)
        {
            labelState.Visible = true;
            string playerName = PlayerNameTextBox.Text;

            //send request to server and all available maps as a response
            _responseFromServer = NetworkUtilities.SendConnectionRequestToServer(_port, playerName);

            //saves received supernode
            _supernodes = _responseFromServer.Supernodes;

            //display all available connection in listBox
            foreach (var sn in _responseFromServer.Supernodes)
            {
                listBoxLobby.Items.Add(sn.Map);
            }


            //starts running engine
            if (_responseFromServer.IsSupernode == true)
            {
                //creates GameForm. Needed for initiating engine (list of Tiles). Player is supernode
                _game = new GameForm(true);

                string fullMapPath = $"Content/{_responseFromServer.MapName}.tmx";

                //Loads all tiles for responsible map and transmit this data to engine's constructor
                var mapTiles = Processor.GetTiles(new TmxMap(fullMapPath));

                //Loads dictionary that holds relationship between Gid from map and custom tileType. needed for change game object to flor
                var gidToTiletypeDict = Processor.GetGidByTileTypeDictionary();

                //creates and starts engine
                _engine = new Engine(_supernodes, mapTiles, _responseFromServer.MapName, gidToTiletypeDict, _port);


                //uses to get remote engine object.RMI stuff
                //TcpChannel clientRemotingChannel = new TcpChannel();
                //ChannelServices.RegisterChannel(clientRemotingChannel, false);

                var provider = new BinaryServerFormatterSinkProvider();
                provider.TypeFilterLevel = TypeFilterLevel.Full;
                var dictionary = new System.Collections.Hashtable();

                // variable port needs to create tcp channel. it is completely different variable than _port. 
                Random rnd = new Random();
                var port = rnd.Next(32001, 47000);
                dictionary["port"] = port;
                dictionary["name"] = $"tcp{port}";
                var clientRemotingChannel = new TcpChannel(dictionary, null, provider);
                ChannelServices.RegisterChannel(clientRemotingChannel, false);

                // connection string vith SN ip and port. 
                string fullUrl = $"tcp://{_ip}:{_port}/remoteEngine";
                _remoteEngine = (RemoteEngine)Activator.GetObject(typeof(RemoteEngine), fullUrl);

                _remoteEngine.CurrentMapName = _responseFromServer.MapName;
                _remoteEngine.Tiles = mapTiles;
            }
            else
            {
                //creates GameForm. Needed for initiating engine (list of Tiles). Player is not supernode
                _game = new GameForm();
            }

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            _game.Dispose();
            base.OnFormClosing(e);
        }
    }
}
