﻿namespace SGame.WinForm
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConnectToServer = new System.Windows.Forms.Button();
            this.settings = new System.Windows.Forms.GroupBox();
            this.buttonJoin = new System.Windows.Forms.Button();
            this.PlayerNameTextBox = new System.Windows.Forms.TextBox();
            this.labelPlayerName = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.labelState = new System.Windows.Forms.Label();
            this.lobbyGroupBox = new System.Windows.Forms.GroupBox();
            this.listBoxLobby = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.settings.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.lobbyGroupBox.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonConnectToServer
            // 
            this.buttonConnectToServer.Location = new System.Drawing.Point(6, 77);
            this.buttonConnectToServer.Name = "buttonConnectToServer";
            this.buttonConnectToServer.Size = new System.Drawing.Size(126, 23);
            this.buttonConnectToServer.TabIndex = 0;
            this.buttonConnectToServer.Text = "Connect to Server";
            this.buttonConnectToServer.UseVisualStyleBackColor = true;
            this.buttonConnectToServer.Click += new System.EventHandler(this.buttonConnectToServer_Click);
            // 
            // settings
            // 
            this.settings.Controls.Add(this.buttonJoin);
            this.settings.Controls.Add(this.PlayerNameTextBox);
            this.settings.Controls.Add(this.labelPlayerName);
            this.settings.Controls.Add(this.buttonConnectToServer);
            this.settings.Location = new System.Drawing.Point(6, 20);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(224, 118);
            this.settings.TabIndex = 2;
            this.settings.TabStop = false;
            this.settings.Text = "Settings";
            // 
            // buttonJoin
            // 
            this.buttonJoin.Location = new System.Drawing.Point(138, 77);
            this.buttonJoin.Name = "buttonJoin";
            this.buttonJoin.Size = new System.Drawing.Size(75, 23);
            this.buttonJoin.TabIndex = 5;
            this.buttonJoin.Text = "Join Game";
            this.buttonJoin.UseVisualStyleBackColor = true;
            this.buttonJoin.Click += new System.EventHandler(this.buttonJoin_Click);
            // 
            // PlayerNameTextBox
            // 
            this.PlayerNameTextBox.Location = new System.Drawing.Point(86, 26);
            this.PlayerNameTextBox.Name = "PlayerNameTextBox";
            this.PlayerNameTextBox.Size = new System.Drawing.Size(127, 20);
            this.PlayerNameTextBox.TabIndex = 4;
            this.PlayerNameTextBox.Text = "Default Player Name";
            // 
            // labelPlayerName
            // 
            this.labelPlayerName.AutoSize = true;
            this.labelPlayerName.Location = new System.Drawing.Point(6, 29);
            this.labelPlayerName.Name = "labelPlayerName";
            this.labelPlayerName.Size = new System.Drawing.Size(74, 13);
            this.labelPlayerName.TabIndex = 3;
            this.labelPlayerName.Text = "PLayer Name:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.labelState);
            this.tabPage1.Controls.Add(this.lobbyGroupBox);
            this.tabPage1.Controls.Add(this.settings);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(751, 187);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelState.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelState.Location = new System.Drawing.Point(173, 156);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(38, 15);
            this.labelState.TabIndex = 6;
            this.labelState.Text = "label1";
            this.labelState.Visible = false;
            // 
            // lobbyGroupBox
            // 
            this.lobbyGroupBox.Controls.Add(this.listBoxLobby);
            this.lobbyGroupBox.Location = new System.Drawing.Point(234, 20);
            this.lobbyGroupBox.Name = "lobbyGroupBox";
            this.lobbyGroupBox.Size = new System.Drawing.Size(502, 118);
            this.lobbyGroupBox.TabIndex = 1;
            this.lobbyGroupBox.TabStop = false;
            this.lobbyGroupBox.Text = "Lobby";
            // 
            // listBoxLobby
            // 
            this.listBoxLobby.FormattingEnabled = true;
            this.listBoxLobby.Location = new System.Drawing.Point(16, 19);
            this.listBoxLobby.Name = "listBoxLobby";
            this.listBoxLobby.Size = new System.Drawing.Size(468, 82);
            this.listBoxLobby.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(759, 213);
            this.tabControl1.TabIndex = 3;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 237);
            this.Controls.Add(this.tabControl1);
            this.Name = "Home";
            this.Text = "Home";
            this.settings.ResumeLayout(false);
            this.settings.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.lobbyGroupBox.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConnectToServer;
        private System.Windows.Forms.GroupBox settings;
        private System.Windows.Forms.Label labelPlayerName;
        private System.Windows.Forms.TextBox PlayerNameTextBox;
        private System.Windows.Forms.Button buttonJoin;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox lobbyGroupBox;
        private System.Windows.Forms.ListBox listBoxLobby;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label labelState;
    }
}