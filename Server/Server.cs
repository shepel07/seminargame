﻿using SharedLibrary;
using SharedLibrary.Networking.Sockets;
using SharedLibrary.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class Server
    {
        private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static List<Socket> _clientSockets = new List<Socket>();
        private const int port = 7777;
        private const int bufferSize = 1024;
        private static byte[] _buffer = new byte[bufferSize];

        //private static IPHostEntry ipHost = Dns.GetHostEntry("localhost");
        //private static IPAddress ipAddr = ipHost.AddressList[1];
        private static IPAddress ipAddr = IPAddress.Parse("127.0.0.1");
        //private static IPAddress ipAddr = IPAddress.Parse("10.0.5.229");

        private static IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);


        /// <summary>
        /// Stores all maps that haven't assigned to super node yet
        /// </summary>
        private static List<string> maps = new List<string> { "m1", "m2" };

        /// <summary>
        /// Dictionary, that holds relation between map and supernode
        /// </summary>
        private static List<Supernode> supernodes = new List<Supernode>();



        static void Main(string[] args)
        {
            RunServer();
            Console.ReadLine();
            CloseAllSockets();
        }

        private static void RunServer()
        {
            Console.Title = "Global Server";
            Console.WriteLine("Runing server . . .");
            _serverSocket.Bind(ipEndPoint);
            _serverSocket.Listen(10);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            Console.WriteLine("Server setup complete");

        }


        /// <summary>
        /// Close all existing clients sockets (connected clients)
        /// </summary>
        private static void CloseAllSockets()
        {
            foreach (Socket socket in _clientSockets)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }

            _serverSocket.Close();
        }

        /// <summary>
        /// Connection accept
        /// </summary>
        private static void AcceptCallback(IAsyncResult AR)
        {
            Socket clientSocket;
            try
            {
                clientSocket = _serverSocket.EndAccept(AR);

            }
            catch (ObjectDisposedException)
            {
                return;
            }
            _clientSockets.Add(clientSocket);
            clientSocket.BeginReceive(_buffer, 0, bufferSize, SocketFlags.None, new AsyncCallback(RecieveCallback), clientSocket);
            Console.WriteLine("Client connected, waiting for request...");
            //SendMessage(clientSocket, "200,400,1");
            //begin to accept new client (aloows to accept more than connection)
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        private static void RecieveCallback(IAsyncResult AR)
        {
            Socket clientSocket = (Socket)AR.AsyncState;
            int received;
            try
            {
                received = clientSocket.EndReceive(AR);

            }
            catch (SocketException)
            {
                Console.WriteLine("Client decided to stop playing");
                clientSocket.Close();
                _clientSockets.Remove(clientSocket);
                return;
            }

            //receives bytes from network and parses them to request connection packet
            byte[] recBuff = new byte[received];
            Array.Copy(_buffer, recBuff, received);
            var requestConnectionData = BytePacket<RequestConnectionPacket>.UnPack(recBuff);

            //port for RMI connection. Comes from client as a parametr in RequestConnectionPacket
            var portFromPlayer = requestConnectionData.Port;

            //variable, that related to prop (isSupernode) in response packat
            bool isSupernode = false;

            //checks if exist not used map
            string currentMap = null;
            if (supernodes.Count < maps.Count)
            {
                //select all used maps from supernode list and compares it with all existing maps
                var usedMaps = supernodes.Select(x => x.Map).ToList();
                var availableMaps = maps.Except(usedMaps).ToList();
                if (availableMaps.Count > 0)
                {
                    //makes client responsible for one map. Tells client to enable SN mode
                    //takes Ip, Port and saves it to supernode class instance
                    var remoteEndPoint = clientSocket.RemoteEndPoint as IPEndPoint;
                    var ip = remoteEndPoint.Address.ToString();
                    var port = portFromPlayer;
                    var newSupernode = new Supernode(availableMaps[0], ip, port);
                    currentMap = availableMaps[0];
                    supernodes.Add(newSupernode);

                    //alows to activate supernode node (supposed to run engine)
                    isSupernode = true;
                }
                else
                {
                    //return just a list of maps
                }
            }

            //creates response packet. Puts into pucket all availble maps for playing. also adds a flas (isSupernode or not). Sends this packet
            var responsePacket = new ResponseConnectionPacket(supernodes, isSupernode, currentMap);
            var responseBytes = responsePacket.Pack();
            SendMessage(clientSocket, responseBytes);

            //begin to accept new client (aloows to accept more than connection)
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        private static void SendCallback(IAsyncResult AR)
        {
            Socket clientSocket = (Socket)AR.AsyncState;
            clientSocket.EndSend(AR);
        }

        private static void SendMessage(Socket currentSocket, byte[] messageBytes)
        {
            currentSocket.BeginSend(messageBytes, 0, messageBytes.Length, SocketFlags.None, new AsyncCallback(SendCallback), currentSocket);
            //currentSocket.BeginReceive(_buffer, 0, bufferSize, SocketFlags.None, RecieveCallback, currentSocket);
        }
    }
}
