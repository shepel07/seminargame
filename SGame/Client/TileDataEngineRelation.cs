﻿using SGame.Client;
using SharedLibrary.GameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.Client
{
    public class TileDataEngineRelation
    {
        public Engine Engine { get; set; }
        public TilesData TilesData { get; set; }

        public TileDataEngineRelation(TilesData data, Engine engine)
        {
            Engine = engine;
            TilesData = data;
        }
    }
}
