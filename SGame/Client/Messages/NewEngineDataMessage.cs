﻿using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGame.Client.Messages
{
    [Serializable]
    public class NewEngineDataMessage : MarshalByRefObject, ICallbackNewEngineData
    {
        public void SendNewEngineData(string remoteEndineIp, int remoteEnginePort)
        {
            GameData.IsNewMap = true;
            var t = $"{remoteEndineIp}:{remoteEnginePort}";
            GameData.NewEngineData = $"{remoteEndineIp}:{remoteEnginePort}";
        }
    }
}
