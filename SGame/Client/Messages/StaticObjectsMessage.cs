﻿using SharedLibrary.GameEngine;
using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGame.Client.Requests
{
    [Serializable]
    public class StaticObjectsMessage : MarshalByRefObject, ICallbackStaticObjects
    {
        public void GetStaticObjects(List<Tile> staticObjects)
        {
            GameData.StaticObjects = staticObjects;
        }
    }
}
