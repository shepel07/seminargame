﻿using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedLibrary.GameEngine;

namespace SGame.Client.Messages
{
    [Serializable]
    public class DynamicObjectsMessage : MarshalByRefObject, ICallbackDynamicObjects
    {
        public void GetDynamicObjects(List<Tile> dynamicObjects)
        {
            GameData.DynamicObjects = dynamicObjects;
        }
    }
}
