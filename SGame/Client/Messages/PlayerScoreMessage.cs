﻿using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGame.Client.Messages
{
    [Serializable]
    public class PlayerScoreMessage : MarshalByRefObject, ICallbackPlayerScore
    {
        public void GetPlayerScore(int score)
        {
            GameData.PlayerScore = score;
        }
    }
}
