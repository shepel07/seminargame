﻿using Microsoft.Xna.Framework.Graphics;
using SharedLibrary.GameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGame.Client
{
    public class TilesData
    {
        #region Properties
        public List<Tile> Tiles { get; set; }
        public Texture2D Tileset { get; set; }
        #endregion

        #region Constructor
        public TilesData(List<Tile> tiles, Texture2D set)
        {
            Tiles = tiles;
            Tileset = set;
        }
        #endregion
    }
}
