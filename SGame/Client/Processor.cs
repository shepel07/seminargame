﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SeminarGame.Client;
using SharedLibrary.GameEngine;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using TiledSharp;

namespace SGame.Client
{
    public class Processor
    {
        #region Variables
        private static CustomVector2 spriteDirection = new CustomVector2(0, 0);
        #endregion

        #region Properties
        public static CustomVector2 SpriteDirection
        {
            get { return spriteDirection; }
            set { spriteDirection = value; }
        }

        /// <summary>
        /// holds relationship between Gid from map and custom tileType
        /// </summary>
        public static Dictionary<TileType, int> GidToTileType { get; set; }
        #endregion

        #region Load Map or Player from tmx
        public static TilesData LoadData(ContentManager content, TmxMap tmxLevelMap)
        {
            var tileset = GetTileSet(content, tmxLevelMap);
            var tiles = GetTiles(tmxLevelMap);
            var data = new TilesData(tiles, tileset);
            return data;
        }

        public static Texture2D GetTileSet(ContentManager content, TmxMap tmxLevelMap)
        {
            return content.Load<Texture2D>(tmxLevelMap.Tilesets[0].Name.ToString());
        }

        /// <summary>
        /// Returns all tiles from map level
        /// </summary>
        /// <param name="tmxLevelMap"></param>
        /// <returns></returns>
        public static List<Tile> GetTiles(TmxMap tmxLevelMap)
        {
            List<Tile> tiles = new List<Tile>();
            GidToTileType = new Dictionary<TileType, int>();
            int width = tmxLevelMap.Tilesets[0].TileWidth;
            int height = tmxLevelMap.Tilesets[0].TileHeight;

            for (var i = 0; i < tmxLevelMap.Layers[0].Tiles.Count; i++)
            {
                string tempGid = tmxLevelMap.Layers[0].Tiles[i].Gid.ToString();
                //retrive type of blocking and gid number. Blocking type should starts from 2, walkable - from 1, player - 3.
                string typeStr = tempGid.Substring(0, 1);
                string gidStr = tempGid.Substring(1);
                int type = Convert.ToInt32(typeStr);
                int gid = Convert.ToInt32(gidStr);
                var tile = tmxLevelMap.Layers[0].Tiles[i];
                var tWidth = tmxLevelMap.TileWidth;
                var tHeight = tmxLevelMap.TileHeight;
                float x = (i % tmxLevelMap.Width) * tWidth;
                float y = (float)Math.Floor(i / (double)tmxLevelMap.Width) * tHeight;
                switch (type)
                {
                    case 1:
                        {
                            tiles.Add(new Tile(gid, new CustomVector2(x, y), TileType.Floor, tWidth, tHeight));
                            if (!GidToTileType.ContainsKey(TileType.Floor))
                                GidToTileType.Add(TileType.Floor, gid);
                            break;
                        }

                    case 2:
                        {
                            tiles.Add(new Tile(gid, new CustomVector2(x, y), TileType.Wall, tWidth, tHeight));
                            if (!GidToTileType.ContainsKey(TileType.Wall))
                                GidToTileType.Add(TileType.Wall, gid);
                            break;

                        }

                    case 3: tiles.Add(new Tile(gid, new CustomVector2(x, y), TileType.Player, tWidth, tHeight)); break;

                    case 4:
                        {
                            tiles.Add(new Tile(gid, new CustomVector2(x, y), TileType.Door, tWidth, tHeight));
                            if (!GidToTileType.ContainsKey(TileType.Door))
                                GidToTileType.Add(TileType.Door, gid);
                            break;
                        }

                    case 5: {
                            tiles.Add(new Tile(gid, new CustomVector2(x, y), TileType.GameObject, tWidth, tHeight));
                            if (!GidToTileType.ContainsKey(TileType.GameObject))
                                GidToTileType.Add(TileType.GameObject, gid);
                            break;
                        } 
                    default: break;
                }
            }
            return tiles;
        }
        #endregion

        /// <summary>
        /// Returns all dictionary that keeps tiletype and gid number relation.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Dictionary<TileType, int> GetGidByTileTypeDictionary()
        {
            return GidToTileType;
        }

        #region Draw Tiles
        public static void DrawTile(SpriteBatch spriteBatch, Texture2D tileset, List<Tile> allTiles)
        {
            foreach (var tile in allTiles)
            {
                var tempW = tile.Width;
                var tempH = tile.Height;
                Rectangle tilesetRec = getTileSetRectangle(tileset, tile.Gid, tempW, tempH);
                spriteBatch.Draw(tileset, new Rectangle((int)tile.Position.X, (int)tile.Position.Y, tempW, tempH), tilesetRec, Color.White);
            }

        }

        private static Rectangle getTileSetRectangle(Texture2D tileset, int gid, int tileWidth, int tileHeight)
        {
            var tilesetTilesWide = tileset.Width / tileWidth;
            var tilesetTilesHigh = tileset.Height / tileHeight;
            int tileFrame = gid - 1;
            int column = tileFrame % tilesetTilesWide;
            int row = (int)Math.Floor((double)tileFrame / (double)tilesetTilesWide);
            Rectangle tilesetRec = new Rectangle(tileWidth * column, tileHeight * row, tileWidth, tileHeight);
            return tilesetRec;
        }
        #endregion

        #region Creates requested number of engines
        //public static List<TileDataEngineRelation> CreateEngines (int number, ContentManager content)
        //{
        //    List<TileDataEngineRelation> dataToEngine = new List<TileDataEngineRelation>();
        //    for (int i = 0; i < number; i++)
        //    {
        //        string path = "Content/m" + (i + 1).ToString() + ".tmx";
        //        TmxMap tmxMap = new TmxMap("Content/m1.tmx");
        //        TilesData tileData = LoadData(content, tmxMap);
        //        string engineId = Guid.NewGuid().ToString();
        //        //Engine engine = new Engine(tileData.Tiles, engineId);
        //        //dataToEngine.Add(new TileDataEngineRelation(tileData, engine));
        //    }
        //    return dataToEngine;
        //}
        #endregion

        //public static void GateGenerate (List<Engine> engines)
        //{
        //    if (engines.Count > 1)
        //    {
        //        for (int i = 0; i < engines.Count; i++)
        //        {
        //            for (int k = i; k < engines.Count; k++)
        //            {
        //                if (k == i || i > k) { continue; }
        //                engines[i].SetNewDoorPosition(engines[i], engines[k], k + i);
        //                engines[k].SetNewDoorPosition(engines[k], engines[i], k + i);
        //            }
        //        }
        //    }
        //}
        
        #region Input Handler and calculation next position
        public static CustomVector2 InputHandler(KeyboardState keyboardState, GameTime gameTime, CustomVector2 spritePosition)
        {
            SpriteDirection = new CustomVector2(0, 0);
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                SpriteDirection += new CustomVector2(0, -3);
            }
            if (keyboardState.IsKeyDown(Keys.Down))
            {
                SpriteDirection += new CustomVector2(0, 3);
            }
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                SpriteDirection += new CustomVector2(3, 0);
            }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                SpriteDirection += new CustomVector2(-3, 0);
            }
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            spriteDirection *= 100;
            spritePosition += (spriteDirection * deltaTime);
            return spritePosition;
        }
        #endregion

    }
}
