﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SGame.Client;
using SGame.Client.Messages;
using SGame.Client.Requests;
using SharedLibrary.GameEngine;
using SharedLibrary.RemoteEngineObjects;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Threading;
using TiledSharp;

namespace SGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    [Serializable]
    public class GameForm : Game
    {
        #region Variables
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private TilesData _playerData;
        private string _playerId;

        /// <summary>
        /// Assign true, if current player is supernode. When this player will jump to other map, 
        /// this variable will allow to change the engine
        /// </summary>
        private bool _isSupernode;
        private SpriteFont spriteFont;
        private Texture2D _tileset;
        private static StaticObjectsMessage _staticObjectsMessage = new StaticObjectsMessage();
        private static AllPlayersPositionsMessage _allPlayersPositionsMessage = new AllPlayersPositionsMessage();
        private static DynamicObjectsMessage _dynamicObjectsMessage = new DynamicObjectsMessage();
        private static PlayerScoreMessage _playerScoreMessage = new PlayerScoreMessage();
        #endregion

        #region Properties
        public IEngine CurrentEngine { get; set; }

        public CustomVector2 CurrentPlayerPosition { get; set; }

        public string MapName { get; set; }

        /// <summary>
        /// Holds all static objects in the map (Walls, Doors, Floor)
        /// </summary>
        public List<Tile> StaticObjects { get; set; }


        public bool Is { get; set; } = false;

        #endregion

        #region Constructor
        public GameForm()
        {
            Window.Title = "2d attempt";
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 560;
            graphics.IsFullScreen = false;
            CurrentPlayerPosition = new CustomVector2(35, 35);
        }
        public GameForm(bool isSupernode) : this()
        {
            _isSupernode = isSupernode;
        }
        #endregion

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>(@"Arial");

            //reads player file and transmits it to processor for loading.
            var tmxPlayer = new TmxMap("Content/player2.tmx");
            _playerData = Processor.LoadData(Content, tmxPlayer);
            //_playerId = CurrentEngine.CreatePlayer(30, 30, _isSupernode);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Is)
            {
                //CurrentEngine.RegisterCallback(new NewEngineDataMessage());
                Is = false;
            }

            if (GameData.IsNewMap && GameData.NewEngineData != ":0")
            {

                #region RMI stuff
                var provider = new BinaryServerFormatterSinkProvider();
                provider.TypeFilterLevel = TypeFilterLevel.Full;
                var dictionary = new System.Collections.Hashtable();

                // variable port needs to create tcp channel. it is completely different variable than _post. 
                Random rnd = new Random();
                var port = rnd.Next(2000, 17000);
                dictionary["port"] = port;
                dictionary["name"] = $"tcp{port}";
                var clientRemotingChannel = new TcpChannel(dictionary, null, provider);
                ChannelServices.RegisterChannel(clientRemotingChannel, false);
                #endregion

                // connection string with SN ip and port. 
                var t = GameData.NewEngineData;
                string fullUrl = $"tcp://{GameData.NewEngineData}/remoteEngine";
                var remoteEngine = (RemoteEngine)Activator.GetObject(typeof(RemoteEngine), fullUrl);

                //assign remote engine to current engine
                CurrentEngine = remoteEngine;

                //registers callback. needed to change map in future
                CurrentEngine.RegisterCallback(new NewEngineDataMessage());

                //_playerId = GameData.PlayerId == "" ? CurrentEngine.CreatePlayer(30, 30, _isSupernode) : GameData.PlayerId;

                if (GameData.PlayerId == "")
                {
                    _playerId = CurrentEngine.CreatePlayer(30, 30, _isSupernode);
                    GameData.PlayerId = _playerId;
                }
                else
                {
                    _playerId = GameData.PlayerId;
                }

                //loads map and gets tile set from it. Invoked only once per new enginee
                var tmxMap = new TmxMap($"Content/{CurrentEngine.CurrentMapName}.tmx");
                _tileset = Processor.GetTileSet(Content, tmxMap);

                //gets all static tiles from engine
                CurrentEngine.GetObjectStaticPositions(_staticObjectsMessage);
                GameData.IsNewMap = false;
                Is = true;
            }

            if (GameData.PlayersData.Count == 0)
            {
                //sets initial player position
                CurrentPlayerPosition = new CustomVector2(55, 55);
            }
            else
            {
                //retrieves current player position from the list of players data by current player Id 
                CurrentPlayerPosition = GameData.PlayersData.FirstOrDefault(x => x.PlayerId == _playerId).PlayerPosition;
            }

            //sets new player position (user input)
            var newPosition = Processor.InputHandler(Keyboard.GetState(), gameTime, CurrentPlayerPosition);
            CurrentEngine.SetPlayerPosition(_playerId, newPosition);

            // updates game state
            CurrentEngine.GetAllPlayersPosition(_allPlayersPositionsMessage);
            CurrentEngine.GetObjectsDynamicPositions(_dynamicObjectsMessage);
            CurrentEngine.GetPlayerScore(_playerId, _playerScoreMessage);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            //draws static objects (walls, doors, etc)
            Processor.DrawTile(spriteBatch, _tileset, GameData.StaticObjects);

            //draws dynamic objects (diamonds, etc)
            Processor.DrawTile(spriteBatch, _tileset, GameData.DynamicObjects);

            //draws player as a tile. currentPos comes as a callback result and consists from playerId and Player position
            foreach (var player in GameData.PlayersData)
            {
                spriteBatch.Draw(_playerData.Tileset, new Vector2(player.PlayerPosition.X, player.PlayerPosition.Y), Color.White);
            }

            //draws message with score
            spriteBatch.DrawString(spriteFont, $"Score: {GameData.PlayerScore}", new Vector2(50, 510), Color.Black);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
