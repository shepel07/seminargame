﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SeminarGame.GameUtilities;
using System;
using System.Collections.Generic;
using TiledSharp;

namespace SeminarGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameForm : Game
    {
            #region Variables
            GraphicsDeviceManager graphics;
            SpriteBatch _spriteBatch;

            private Player _player;
            private List<Engine> _engines;
            private List<Map> _maps;
            private List<MapEngineRelation> _mapEngine;

            private Engine _engine1;
            private Engine _engine2;
            private Engine _engine3;
            private Engine _currentEngine;
            private SpriteFont spriteFont;
            //checks if intersection has occurred
            private bool isIntersected = false;
            #endregion

            #region Properties
            public List<Engine> Engines
            {
                get { return _engines; }
                set { _engines = value; }
            }
            #endregion

            #region Constructor
            public GameForm()
            {
                Window.Title = "No Network Connection";
                graphics = new GraphicsDeviceManager(this);
                Content.RootDirectory = "Content";
                graphics.PreferredBackBufferWidth = 1280;
                graphics.PreferredBackBufferHeight = 940;
                graphics.IsFullScreen = false;
                _engines = new List<Engine>();
                _maps = new List<Map>();
                _mapEngine = new List<MapEngineRelation>();

                //_currentEngine = _engine1 = new Engine("mapp1");
                //_player = new Player(_engine1, new Vector2(100, 100));
                //_engine1.CurrentPlayer = _player;
                //_engines.Add(_engine1);
                //_engine2 = new Engine("mapp2");
                //_engines.Add(_engine2);
                //_engine3 = new Engine("mapp3");
                //_engines.Add(_engine3);

            }
            #endregion



            /// <summary>
            /// Allows the game to perform any initialization it needs to before starting to run.
            /// This is where it can query for any required services and load any non-graphic
            /// related content.  Calling base.Initialize will enumerate through any components
            /// and initialize them as well.
            /// </summary>
            protected override void Initialize()
            {
                for (int i = 0; i < 3; i++)
                {
                    string mapName = "mapp" + (i + 1).ToString();
                    Map map = new Map(Content, mapName);
                    Engine engine = new Engine(map.Tiles);
                    _mapEngine.Add(new MapEngineRelation(map, engine));
                    _maps.Add(map);
                }
                _player = new Player(Content, new Vector2(100, 100), "Alex");
                _mapEngine[0].Engine.CurrentPlayer = _player;

                base.Initialize();
            }

            /// <summary>
            /// LoadContent will be called once per game and is the place to load
            /// all of your content.
            /// </summary>
            protected override void LoadContent()
            {
                // Create a new SpriteBatch, which can be used to draw textures.
                _spriteBatch = new SpriteBatch(GraphicsDevice);
                spriteFont = Content.Load<SpriteFont>(@"Arial");





                //generates gates based on number of engines
                if (_engines.Count > 1)
                {
                    for (int i = 0; i < _engines.Count; i++)
                    {
                        for (int k = i; k < _engines.Count; k++)
                        {
                            if (k == i || i > k) { continue; }
                            _engines[i].GetNewDoorPosition(_engines[i], _engines[k], k + i);
                            _engines[k].GetNewDoorPosition(_engines[k], _engines[i], k + i);
                        }
                    }
                }
            }

            /// <summary>
            /// UnloadContent will be called once per game and is the place to unload
            /// game-specific content.
            /// </summary>
            protected override void UnloadContent()
            {
                // TODO: Unload any non ContentManager content here
            }

            /// <summary>
            /// Allows the game to run logic such as updating the world,
            /// checking for collisions, gathering input, and playing audio.
            /// </summary>
            /// <param name="gameTime">Provides a snapshot of timing values.</param>
            protected override void Update(GameTime gameTime)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();

                foreach (var engine in _engines)
                {
                    foreach (var door in engine.Doors)
                    {
                        if (door.GateTriggered)
                        {
                            _currentEngine = door.NextEngine;
                            _currentEngine.CurrentPlayer = door.CurrentEngine.CurrentPlayer;
                            _currentEngine.CurrentPlayer.SpritePosition = _currentEngine.SetPlayerPositionAfterTeleporting(door.GateId);
                        }
                    }
                }
                //_currentEngine.Update(gameTime);



                foreach (var item in _mapEngine)
                {
                    if (item.Engine.CurrentPlayer != null)
                    {
                        item.Engine.InputHandler(Keyboard.GetState());
                        item.Engine.CurrentPlayer.Update(gameTime);
                    }
                }

                base.Update(gameTime);
            }

            /// <summary>
            /// This is called when the game should draw itself.
            /// </summary>
            /// <param name="gameTime">Provides a snapshot of timing values.</param>
            protected override void Draw(GameTime gameTime)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);

                _spriteBatch.Begin();

                _mapEngine[0].Map.Draw(_spriteBatch);
                _player.Draw(_spriteBatch);






                if (isIntersected)
                {
                    string warningText = "Warning!!!You are going to leave this area!!!";
                    //spriteBatch.DrawString(spriteFont, warningText, new Vector2(20, 20), Color.White);
                }

                //_currentEngine.Draw(_spriteBatch);
                _spriteBatch.DrawString(spriteFont, "PlayerDirection " + _mapEngine[0].Engine.CurrentPlayer.SpriteDirection, new Vector2(20, 500), Color.Black);
                _spriteBatch.DrawString(spriteFont, "PlayerPosition " + _mapEngine[0].Engine.CurrentPlayer.SpritePosition, new Vector2(20, 530), Color.Black);
                //_spriteBatch.DrawString(spriteFont, "tile.Position " + _currentEngine.CurrentPlayer.PlayerTile.Position, new Vector2(20, 530), Color.Black);
                //_spriteBatch.DrawString(spriteFont, "Sprite Direction" + _currentEngine.CurrentPlayer.SpriteDirection, new Vector2(20, 600), Color.Black);
                //_spriteBatch.DrawString(spriteFont, "Sprite Position" + _currentEngine.CurrentPlayer.SpritePosition, new Vector2(20, 650), Color.Black);
                //_spriteBatch.DrawString(spriteFont, "Move" + _currentEngine.Move, new Vector2(20, 700), Color.Black);
                _spriteBatch.End();

                base.Draw(gameTime);
            }
        }
    }
}
