﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities.TileUtilities
{
    public class Door : Tile, IGate
    {



        #region Variables
        private bool _gateTriggered;
        #endregion

        #region Properties
        public Engine CurrentEngine { get; set; }
        public Engine NextEngine { get; set; }
        public string GateId { get; set; }
        public bool GateTriggered
        {
            get
            {
                if (_gateTriggered) { _gateTriggered = false; return true; }
                return false;
            }
            set { _gateTriggered = value; }
        }
        #endregion

        #region Constructors

        public Door(string id, int gid, Vector2 position, int width, int height) : base(id, gid, position, width, height)
        {
        }

        public Door(string id, int gid, Vector2 position, int width, int height, 
            Engine currentEngine, Engine nextEngine, string gateId) : base(id, gid, position, width, height)
        {
            CurrentEngine = currentEngine;
            NextEngine = nextEngine;
            GateId = gateId;
        }
        #endregion


        public override void Draw(SpriteBatch spriteBatch, Texture2D tileset)
        {
            base.Draw(spriteBatch, tileset);
        }
    }
}
