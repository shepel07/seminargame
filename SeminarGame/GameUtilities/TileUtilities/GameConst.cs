﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities.TileUtilities
{
    public static class GameConst
    {
        public static int tileWidth = 32;
        public static int tileHeight = 32;
        public static int gameWidth = 640;
        public static int gameHeight = 480;

    }
}
