﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities.TileUtilities
{
    public class Floor : Tile
    {
        public Floor(string id, int gid, Vector2 position, int width, int height) : base(id, gid, position, width, height) { }

        public override void Draw(SpriteBatch spriteBatch, Texture2D tileset)
        {
            base.Draw(spriteBatch, tileset);
        }
    }
}
