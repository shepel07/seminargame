﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities.TileUtilities
{
    public class Tile
    {

        #region Variables
        private string _id;
        private int _gid;
        private Vector2 _position;
        private int _width;
        private int _heidth;
        #endregion

        #region Properties
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Gid
        {
            get { return _gid; }
            set { _gid = value; }
        }

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        #endregion

        #region Constructor
        public Tile(string id, int gid, Vector2 position, int width, int height)
        {
            _id = id;
            _gid = gid;
            _position = position;
            _width = width;
            _heidth = height;
        }
        #endregion

        public virtual void Draw(SpriteBatch spriteBatch, Texture2D tileset)
        {
            Rectangle tilesetRec = getTileSetRectangle(tileset);
            spriteBatch.Draw(tileset, new Rectangle((int)Position.X, (int)Position.Y, _width, _heidth), tilesetRec, Color.White);
        }
        
        public Rectangle getTileSetRectangle(Texture2D tileset)
        {
            var tilesetTilesWide = tileset.Width / _width;
            var tilesetTilesHigh = tileset.Height / _heidth;
            int tileFrame = Gid - 1;
            int column = tileFrame % tilesetTilesWide;
            int row = (int)Math.Floor((double)tileFrame / (double)tilesetTilesWide);
            Rectangle tilesetRec = new Rectangle(_width * column, _heidth * row, _width, _heidth);
            return tilesetRec;
        }

    }
}
