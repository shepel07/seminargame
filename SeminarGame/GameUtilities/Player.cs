﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SeminarGame.GameUtilities.TileUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledSharp;

namespace SeminarGame.GameUtilities
{
    public class Player : AnimatedSprite
    {


        #region Variables
        private Guid _id;
        private float _playerSpeed = 100;
        private TmxMap _tmxPlayer;
        private Texture2D _tileset;
        private Tile _playerTile;
        private Vector2 _position;
        #endregion

        #region Properties
        public int Score { get; set; } = 0;
        public string Name { get; set; }

        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Tile PlayerTile
        {
            get { return _playerTile; }
            set { _playerTile = value; }
        }

        #endregion

        #region Constructor
        public Player(ContentManager content, Vector2 position, string name) : base(position)
        {
            Id = Guid.NewGuid();
            _position = position;
            Name = name;
            FPS = 10;
            LoadContent(content);
        }
        #endregion

        /// <summary>
        /// Load player content
        /// </summary>
        private void LoadContent(ContentManager content)
        {
            //spriteTexture = content.Load<Texture2D>("nevanda_nethack");
            //CurrentMap = new Map(content, "map1");
            ////Adds all the players animations
            //AddAnimation(1);

            string fullName = "Content/" + "player2" + ".tmx";
            _tmxPlayer = new TmxMap(fullName);
            _tileset = content.Load<Texture2D>(_tmxPlayer.Tilesets[0].Name.ToString());
            int gid = _tmxPlayer.Layers[0].Tiles[0].Gid;
            _playerTile = new Tile(Guid.NewGuid().ToString(),
                gid,
                new Vector2(_position.X, _position.Y),
                _tmxPlayer.TileWidth,
                _tmxPlayer.TileHeight);
        }

        public override void Update(GameTime gameTime)
        {
            //stops player's movement
            //spriteDirection = Vector2.Zero;
            //_engine.InputHandler(Keyboard.GetState());

            //Calculates how many seconds that has passed since last iteration of Update
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //set player's speed
            spriteDirection *= _playerSpeed;

            //Makes the movement framerate independent
            spritePosition += (spriteDirection * deltaTime);

            _playerTile.Position = SpritePosition;

            base.Update(gameTime);
        }

        public void SetSpriteDirection(Vector2 value)
        {
            spriteDirection = value;
        }

        public void addSpriteDirection(Vector2 value)
        {
            spriteDirection += value;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _playerTile.Draw(spriteBatch, _tileset);
        }

    }
}
