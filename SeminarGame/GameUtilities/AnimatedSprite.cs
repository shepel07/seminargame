﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities
{
    public abstract class AnimatedSprite
    {

        #region Variables
        /// <summary>
        /// Sprite position
        /// </summary>
        protected Vector2 spritePosition;

        /// <summary>
        /// Sprite's texture
        /// </summary>
        protected Texture2D spriteTexture;

        /// <summary>
        /// Stores player animation
        /// </summary>
        private Rectangle[] spriteRectangles;

        /// <summary>
        /// Index of Requested frame from animation
        /// </summary>
        private int frameIndex;

        /// <summary>
        /// How much time has passed since last frame
        /// </summary>
        private double timeElapsed;

        /// <summary>
        /// How much time needs to update the frame
        /// </summary>
        private double timeToUpdate;

        /// <summary>
        /// Next sprite's direction
        /// </summary>
        protected Vector2 spriteDirection = Vector2.Zero;
        #endregion


        #region Properties

        public Vector2 SpriteDirection
        {
            get { return spriteDirection; }
            set { spriteDirection = value; }
        }


        public Texture2D SpriteTexture
        {
            get { return spriteTexture; }
            set { spriteTexture = value; }
        }

        public Vector2 SpritePosition
        {
            get { return spritePosition; }
            set { spritePosition = value; }
        }

        /// <summary>
        /// Our time per frame is equal to 1 divided by frames per second(we are deciding FPS)
        /// </summary>
        public int FPS
        {
            set { timeToUpdate = (1f / value); }
        }
        #endregion

        #region Constructor
        public AnimatedSprite(Vector2 position)
        {
            spritePosition = position;
        }
        #endregion

        /// <summary>
        /// Adds an animation to the AnimatedSprite
        /// </summary>
        /// <param name="frames">Total number of sprites in spritesheet</param>
        public void AddAnimation(int frames)
        {
            //sprite's width in spritesheet
            int width = spriteTexture.Width / frames;
            spriteRectangles = new Rectangle[frames];
            for (int i = 0; i < frames; i++)
            {
                spriteRectangles[i] = new Rectangle(i * width, 0, width, spriteTexture.Height);
            }
        }


        /// <summary>
        /// Makes animated sprite work. Updates picture
        /// </summary>
        public virtual void Update(GameTime gameTime)
        {
            timeElapsed += gameTime.ElapsedGameTime.TotalSeconds;
            if (timeElapsed > timeToUpdate)
            {
                timeElapsed -= timeToUpdate;

                if (spriteRectangles != null)
                {
                    //next frame
                    if (frameIndex > spriteRectangles.Length - 1)
                    {
                        frameIndex++;
                    }
                    else
                    {
                        frameIndex = 0;

                    }
                }
            }
        }

        /// <summary>
        /// Draw our sprite
        /// </summary>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(spriteTexture, spritePosition, spriteRectangles[frameIndex], Color.White);
        }


    }
}
