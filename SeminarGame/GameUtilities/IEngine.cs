﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities
{
    interface IEngine
    {
        Guid Id { get; set; }

        void Start();
        void Stop();
    }
}
