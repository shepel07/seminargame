﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SeminarGame.GameUtilities.TileUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities
{
    public class Engine : IEngine
    {
        #region Variables
        private Map levelMap;
        private string _mapName;
        private Move _move;
        private List<Door> _doors;
        private List<Tile> _tiles;
        #endregion

        #region Properties
        public Move Move
        {
            get { return _move; }
            set { _move = value; }
        }

        public List<Door> Doors
        {
            get { return _doors; }
            set { _doors = value; }
        }
        //public Player CurrentPlayer { get; set; }
        public List<Player> Players { get; set; }
        public Map LevelMap
        {
            get { return levelMap; }
            set { levelMap = value; }
        }

        public Guid Id { get; set; }
        #endregion

        #region Constructors
        public Engine(List<Tile> tiles) {
            Id = Guid.NewGuid();
            _tiles = tiles;
            Players = new List<Player>();
            _doors = new List<Door>();
            Run();
        }
        public Engine(string mapName)
        {
            _mapName = mapName;
            Players = new List<Player>();
            _doors = new List<Door>();
            Id = Guid.NewGuid();
            Run();
        }
        #endregion


        private void Run()
        {
            var runThread = new Thread(() =>
            {
                while (true)
                {
                    //if (CurrentPlayer != null)
                        //InputHandler(Keyboard.GetState());
                }

            });
            runThread.Start();
        }


        //public void LoadContent(ContentManager content)
        //{
        //    //initiates window level map and player
        //    levelMap = new Map(content, _mapName);
        //    if (CurrentPlayer != null)
        //        CurrentPlayer.LoadContent(content);
        //}

        //public void Update(GameTime gameTime)
        //{
        //    InputHandler(Keyboard.GetState());
        //    CurrentPlayer.Update(gameTime);
        //    TileCollision();
        //}

        //public void Draw(SpriteBatch spriteBatch)
        //{
        //    levelMap.Draw(spriteBatch);
        //    CurrentPlayer.Draw(spriteBatch);
        //}

        public void AddDoor(Door newDoor)
        {
            _doors.Add(newDoor);
        }


        /// <summary>
        /// gets random wall position and replaces the wall on the door
        /// </summary>
        public void GetNewDoorPosition(Engine currentEngine, Engine nextEngine, int doorId)
        {
            //gets all wall tile in array
            var walls = levelMap.Tiles.FindAll(x => x is Wall);
            //finds random tile from array
            var indexInWalls = new Random().Next(walls.Count);
            //takes this tile from array
            var changedTile = walls[indexInWalls];
            //creates new door tile which will replace random wall tile
            Door newDoor = new Door(changedTile.Id, 845, changedTile.Position, levelMap.TileWidth, levelMap.TileHeight, currentEngine, nextEngine, doorId.ToString());
            //finds current wall in general array and returns index
            var index = levelMap.Tiles.FindIndex(x => x.Id == changedTile.Id);
            //replaces the door in general array
            levelMap.Tiles[index] = newDoor;
            AddDoor(newDoor);
        }

        //not final implementation
        public void RemoveDoor(Door door)
        {
            _doors.Remove(door);
        }

        public void InputHandler(KeyboardState keyboardState)
        {
            CurrentPlayer.SetSpriteDirection(new Vector2(0, 0));
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                //Move player up
                CurrentPlayer.addSpriteDirection(new Vector2(0, -3));
                _move = Move.Up;
            }
            if (keyboardState.IsKeyDown(Keys.Down))
            {
                //Move player Down
                CurrentPlayer.addSpriteDirection(new Vector2(0, 3));
                _move = Move.Down;
            }
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                //Move player Right
                CurrentPlayer.addSpriteDirection(new Vector2(3, 0));
                _move = Move.Right;
            }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                //Move player Left
                CurrentPlayer.addSpriteDirection(new Vector2(-3, 0));
                _move = Move.Left;
            }

            if (keyboardState.IsKeyDown(Keys.Left) && keyboardState.IsKeyDown(Keys.Up))
            {
                CurrentPlayer.SetSpriteDirection(new Vector2(0, 0));
                _move = Move.LeftUp;
                //Move player left and up simultaneously
                CurrentPlayer.addSpriteDirection(new Vector2(-3, -3));
            }
        }

        /// <summary>
        /// Generatetes new player position
        /// </summary>
        /// <param name="gateId"></param>
        /// <returns>player position coordinates after teleporting</returns>
        public Vector2 SetPlayerPositionAfterTeleporting(string gateId)
        {
            Vector2 gate = _doors.FirstOrDefault(x => x.GateId == gateId).Position;

            //checks if the door in map boundary
            if (gate.X == 0) { return new Vector2(GameConst.tileWidth, gate.Y); }
            if (gate.X == GameConst.gameWidth - GameConst.tileWidth) { return new Vector2(gate.X - GameConst.tileWidth, gate.Y); }
            if (gate.Y == 0) { return new Vector2(gate.X, GameConst.tileHeight); }
            if (gate.Y == GameConst.gameHeight - GameConst.tileHeight) { return new Vector2(gate.X, gate.Y - GameConst.tileHeight); }

            //checks if the door inside map, but still in the wall
            //ttakes 4 nearest tiles and checks their type.
            var tileUp = levelMap.Tiles.FirstOrDefault(x => x.Position == new Vector2(gate.X, gate.Y - GameConst.tileHeight));
            var tileDown = levelMap.Tiles.FirstOrDefault(x => x.Position == new Vector2(gate.X, gate.Y + GameConst.tileHeight));
            var tileRight = levelMap.Tiles.FirstOrDefault(x => x.Position == new Vector2(gate.X + GameConst.tileWidth, gate.Y));
            var tileLeft = levelMap.Tiles.FirstOrDefault(x => x.Position == new Vector2(gate.X - GameConst.tileWidth, gate.Y));
            List<Tile> tempNileList = new List<Tile>() { tileUp, tileDown, tileRight, tileLeft };
            foreach (var tile in tempNileList)
            {
                if (tile is Door || tile is Wall) { continue; }
                else
                {
                    return new Vector2(tile.Position.X, tile.Position.Y);
                }
            }
            return new Vector2(100, 100);
        }

        public void TileCollision()
        {
            //foreach (var tile in levelMap.Tiles)
            for (int i = 0; i < levelMap.Tiles.Count; i++)

            {
                //checks door collision
                if ((Math.Abs(levelMap.Tiles[i].Position.X - CurrentPlayer.PlayerTile.Position.X) <= 20 &&
                    Math.Abs(levelMap.Tiles[i].Position.Y - CurrentPlayer.PlayerTile.Position.Y) <= 20) &&
                    levelMap.Tiles[i] is Door)
                {
                    Door tile = (Door)levelMap.Tiles[i];
                    tile.GateTriggered = true;
                }

                //checks wall collision
                //if ((Math.Abs(levelMap.Tiles[i].Position.X - CurrentPlayer.PlayerTile.Position.X) <= 32 &&
                //    Math.Abs(levelMap.Tiles[i].Position.Y - CurrentPlayer.PlayerTile.Position.Y) <= 32) &&
                //    levelMap.Tiles[i] is Wall)
                //{
                //    CurrentPlayer.PlayerTile.Position = new Vector2(levelMap.Tiles[i].Position.X - 30, CurrentPlayer.PlayerTile.Position.Y);
                //}

                //checks diamond collision
                if ((Math.Abs(levelMap.Tiles[i].Position.X - CurrentPlayer.PlayerTile.Position.X) <= 15 &&
                    Math.Abs(levelMap.Tiles[i].Position.Y - CurrentPlayer.PlayerTile.Position.Y) <= 15) &&
                    levelMap.Tiles[i] is Diamond)
                {
                    levelMap.Tiles[i] = new Floor(levelMap.Tiles[i].Id, 1081, levelMap.Tiles[i].Position, 32, 32);
                    CurrentPlayer.Score += 1;
                }
            }
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }
    }
}

