﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities
{
    interface IGate
    {
        Engine CurrentEngine { get; set; }
        Engine NextEngine { get; set; }
        string GateId { get; set; }
        bool GateTriggered { get; }

    }
}
