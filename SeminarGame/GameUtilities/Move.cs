﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities
{
    [Flags()]
    public enum Move
    {
        Idle, Right, Left, Up, Down,
        RightUp, RightDown, LeftUp, LeftDown
    }
}