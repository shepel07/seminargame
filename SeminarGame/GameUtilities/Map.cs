﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SeminarGame.GameUtilities;
using SeminarGame.GameUtilities.TileUtilities;
using System;
using System.Collections.Generic;
using TiledSharp;

namespace SeminarGame.GameUtilities
{
    public class Map
    {

        #region TileSet Variables
        private string _name;
        private List<Tile> _tiles;
        private ContentManager _content;
        private TmxMap _tmxLevelMap;
        private Texture2D _tileset;
        private int _tileWidth;
        private int _tileHeight;
        #endregion

        #region Properties
        public TmxMap TmxLevelMap
        {
            get { return _tmxLevelMap; }
        }

        public List<Tile> Tiles
        {
            get { return _tiles; }
            set { _tiles = value; }
        }

        public int TileWidth
        {
            get { return _tileWidth; }
        }

        public int TileHeight
        {
            get { return _tileHeight; }
        }

        #endregion

        #region Constructor
        public Map(ContentManager content, string name)
        {
            _name = name;
            _content = content;
            _tiles = new List<Tile>();
            LoadMap(content, name);
        }
        #endregion

        private void LoadMap(ContentManager content, string fileName)
        {
            string fullName = "Content/" + fileName + ".tmx";
            _tmxLevelMap = new TmxMap(fullName);
            _tileset = content.Load<Texture2D>(_tmxLevelMap.Tilesets[0].Name.ToString());
            _tileWidth = _tmxLevelMap.Tilesets[0].TileWidth;
            _tileHeight = _tmxLevelMap.Tilesets[0].TileHeight;
            _tiles = GetTiles(_tmxLevelMap, _tileWidth, _tileHeight);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (var i = 0; i < _tiles.Count; i++)
            {
                _tiles[i].Draw(spriteBatch, _tileset);
            }
        }

        private List<Tile> GetTiles(TmxMap tmxLevelMap, int width, int height)
        {
            List<Tile> tiles = new List<Tile>();

            for (var i = 0; i < tmxLevelMap.Layers[0].Tiles.Count; i++)
            {
                int gid = tmxLevelMap.Layers[0].Tiles[i].Gid;
                var tile = tmxLevelMap.Layers[0].Tiles[i];
                Guid guid = Guid.NewGuid();
                float x = (i % _tmxLevelMap.Width) * _tmxLevelMap.TileWidth;
                float y = (float)Math.Floor(i / (double)_tmxLevelMap.Width) * _tmxLevelMap.TileHeight;
                switch (gid)
                {
                    case 841: tiles.Add(new Wall(guid.ToString(), gid, new Vector2(x, y), width, height)); break;
                    //case 845: tiles.Add(new Door(guid.ToString(), gid, new Vector2(x, y), width, height)); break;
                    case 1081: tiles.Add(new Floor(guid.ToString(), gid, new Vector2(x, y), width, height)); break;
                    case 803: tiles.Add(new Diamond(guid.ToString(), gid, new Vector2(x, y), width, height)); break;
                    default: break;
                }
            }
            return tiles;
        }

    }
}
