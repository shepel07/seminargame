﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeminarGame.GameUtilities
{
    public class MapEngineRelation
    {
        public Engine Engine { get; set; }
        public Map Map { get; set; }

        public MapEngineRelation (Map map, Engine engine)
        {
            Engine = engine;
            Map = map;
        }
    }
}
