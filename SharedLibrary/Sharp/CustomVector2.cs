﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.Sharp
{
    [Serializable]
    public struct CustomVector2
    {

        public float X { get; set; }
        public float Y { get; set; }


        public CustomVector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(CustomVector2 value1, CustomVector2 value2)
        {
            if (value1 == null || value2 == null)
                return false;
            return (value1.X == value2.X) && (value1.Y == value2.Y);
        }
        public static bool operator !=(CustomVector2 value1, CustomVector2 value2)
        {
            return true;
        }

        public static CustomVector2 operator +(CustomVector2 value1, CustomVector2 value2)
        {
            var x = value1.X + value2.X;
            var y = value1.Y + value2.Y;
            return new CustomVector2(x,y);
        }

        public static CustomVector2 operator *(CustomVector2 value1, float number)
        {
            var x = value1.X * number;
            var y = value1.Y * number;
            return new CustomVector2(x, y);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool Equals(CustomVector2 value1, CustomVector2 value2)
        {
            if (value1 == null || value2 == null)
                return false;

            return (value1.X == value2.X) && (value1.Y == value2.Y);
        }
    }
}
