﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary
{
    [Serializable]
    public class Supernode
    {

        #region Properties
        public string Ip{ get; set; }
        public string Map { get; set; }
        public int Port { get; set; }

        #endregion

        #region Constructor
        public Supernode() { }
        public Supernode(string map, string ip, int port)
        {
            Map = map;
            Ip = ip;
            Port = port;
        }
        #endregion
    }
}