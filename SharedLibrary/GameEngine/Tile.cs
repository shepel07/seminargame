﻿using Microsoft.Xna.Framework;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    [Serializable]
    public class Tile
    {
        #region Variables
        private int _gid;
        private CustomVector2 _position;
        private TileType _type;
        private int _width;
        private int _height;
        #endregion

        #region Properties
        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public TileType TypeofTile
        {
            get { return _type; }
            set { _type = value; }
        }

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }


        public int Gid
        {
            get { return _gid; }
            set { _gid = value; }
        }

        public CustomVector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        #endregion

        #region Constructor
        public Tile(int gid, CustomVector2 position, TileType type, int width, int height)
        {
            _gid = gid;
            _position = position;
            _type = type;
            _width = width;
            _height = height;
        }
        #endregion

    }
}
