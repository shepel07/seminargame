﻿using Microsoft.Xna.Framework;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    [Serializable]
    public class Player
    {
        #region Properties
        public string Id { get; set; }
        public CustomVector2 Position { get; set; }
        public PlayerType TypeOfPlayer { get; set; }
        public int TileWidth { get; set; }
        public int TileHeight { get; set; }
        public int Score { get; set; } = 0;
        #endregion

        #region Constructors
        public Player(string id, CustomVector2 position, PlayerType type, int tileWidth, int tileHeight)
        {
            Id = id;
            Position = position;
            TypeOfPlayer = type;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
        }
        #endregion
    }
}