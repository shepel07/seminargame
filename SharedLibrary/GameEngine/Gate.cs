﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    public class Gate : IGate
    {

        #region Variables
        #endregion

        #region Properties
        public Engine CurrentEngine { get; set; }
        public Engine NextEngine { get; set; }
        public string GateId { get; set; }
        public bool GateTriggered { get; }
        public byte[] befferRx { get; set; }
        public byte[] befferTx { get; set; }
        #endregion

        #region Constructor
        public Gate(Engine currentEngine, Engine nextEngine, string gateId)
        {
            CurrentEngine = currentEngine;
            NextEngine = nextEngine;
            GateId = gateId;
        }
        #endregion
    }
}
