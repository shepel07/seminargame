﻿using Microsoft.Xna.Framework;
using SharedLibrary.RemoteEngineObjects;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    public interface IEngine
    {
        #region Properties
        string CurrentMapName { get; set; }
        Dictionary<TileType, int> GidToTileType { get; set; }
        #endregion

        #region Methods
        string CreatePlayer(int tileWidth, int tileHeight, bool isSupernode);
        void SetPlayerPosition(string playerId, CustomVector2 newPosition);
        void GetObjectStaticPositions(ICallbackStaticObjects callback);
        void GetAllPlayersPosition(ICallbackAllPlayersPositions callback);
        void GetObjectsDynamicPositions(ICallbackDynamicObjects callback);
        void GetPlayerScore(string playerId, ICallbackPlayerScore callback);
        void PutPlayerToRxBuffer(Player player);
        void RegisterCallback(ICallbackNewEngineData mes);
        #endregion
    }
}
