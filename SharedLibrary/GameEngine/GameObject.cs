﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    public class GameObject
    {
        #region Properties
        public bool IsExist { get; set; }
        public Vector2 ObjectPosition { get; set; }
        #endregion

        #region Constructors
        public GameObject (bool isExist, Vector2 objectPosition)
        {
            IsExist = isExist;
            ObjectPosition = objectPosition;
        }
        #endregion

    }
}
