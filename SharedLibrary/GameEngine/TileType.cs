﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    public enum TileType
    {
        Wall = 4, Floor, Player, Door, GameObject, FloorAfterGameObject
    }
}