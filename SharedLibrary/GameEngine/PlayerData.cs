﻿using Microsoft.Xna.Framework;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    /// <summary>
    /// Intended for retrieving current player in game form. 
    /// Eliminates needs to create new request like GetCurrentPlayerPosition.
    /// </summary>
    [Serializable]
    public class PlayerData
    {
        #region Properties
        public string PlayerId{ get;set; }
        public CustomVector2 PlayerPosition { get; set; }
        #endregion

        #region Constructor
        public PlayerData(string id, CustomVector2 pos)
        {
            PlayerId = id;
            PlayerPosition = pos;
        }
        #endregion
    }
}
