﻿using Microsoft.Xna.Framework;
using SharedLibrary.GameEngine.Actions;
using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    public class Engine/* : IEngine*/
    {
        private RemoteEngine _remoteEngine;
        public Engine(List<Supernode> supernodes, List<Tile> tiles, string mapName, Dictionary<TileType, int> dict, int port)
        {
            //RMI stuff

            _remoteEngine = new RemoteEngine(supernodes, tiles, mapName, dict, port);

            //BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
            //provider.TypeFilterLevel = TypeFilterLevel.Full;
            //System.Collections.IDictionary dictionary = new System.Collections.Hashtable();
            //dictionary["port"] = port;
            //dictionary["name"] = $"tcp{port}";
            //TcpChannel remotingChannel = new TcpChannel(dictionary, null, provider);
            //ChannelServices.RegisterChannel(remotingChannel, false);


            var provider = new BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = TypeFilterLevel.Full;
            var dictionary = new System.Collections.Hashtable();
            dictionary["port"] = port;
            dictionary["name"] = $"tcp{port}";
            var remotingChannel = new TcpChannel(dictionary, null, provider);
            ChannelServices.RegisterChannel(remotingChannel, false);

            WellKnownServiceTypeEntry remoteObject = new WellKnownServiceTypeEntry(typeof(RemoteEngine), "remoteEngine", WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(remoteObject);
        }


        //#region Variables
        //private bool _isRunning;
        //private List<Gate> _gates;

        ///// <summary>
        ///// Holds and process all action from client
        ///// </summary>
        //private static ConcurrentQueue<Object> _queue;

        ///// <summary>
        ///// List of players on current map
        ///// </summary>
        //private List<Player> _players;
        //#endregion

        //#region Properties
        //public string CurrentMapName { get; set; }
        //public Vector2 InitPositions { get; set; }

        //public static ConcurrentQueue<Object> Queue
        //{
        //    get { return _queue; }
        //    set { _queue = value; }
        //}


        ///// <summary>
        ///// holds relationship between Gid from map and custom tileType
        ///// </summary>
        //public static Dictionary<TileType, int> GidToTileType { get; set; }

        //public List<Gate> Gates
        //{
        //    get { return _gates; }
        //    set { _gates = value; }
        //}

        //public List<Tile> Tiles { get; set; }

        //public bool IsRunning
        //{
        //    get { return _isRunning; }
        //    set { _isRunning = value; }
        //}
        //#endregion

        //#region Constructors
        //public Engine(List<Tile> tiles, string mapName, Dictionary<TileType, int> dict, int port)
        //{
        //    Tiles = tiles;
        //    CurrentMapName = mapName;
        //    GidToTileType = dict;
        //    _gates = new List<Gate>();
        //    _queue = new ConcurrentQueue<Object>();
        //    _players = new List<Player>();
        //    Start();

        //    //RMI stuff
        //    System.Collections.IDictionary dictionary = new System.Collections.Hashtable();
        //    dictionary["port"] = port;
        //    dictionary["name"] = $"tcp{port}";
        //    TcpChannel remotingChannel = new TcpChannel(dictionary, null, null);

        //    ChannelServices.RegisterChannel(remotingChannel, false);
        //    WellKnownServiceTypeEntry remoteObject = new WellKnownServiceTypeEntry(typeof(RemoteEngine), "remoteEngine", WellKnownObjectMode.Singleton);
        //    RemotingConfiguration.RegisterWellKnownServiceType(remoteObject);
        //}
        //#endregion


        //private void Start()
        //{
        //    var runthread = new Thread(() =>
        //    {
        //        // variable, that allows retrieve first element from queue
        //        object itemInQueue;
        //        while (true)
        //        {
        //            _queue.TryDequeue(out itemInQueue);
        //            Process(itemInQueue);
        //        }
        //    });
        //    runthread.Start();
        //}

        ///// <summary>
        ///// Process messages from the queue
        ///// </summary>
        ///// <param name="itemInQueue"></param>
        //private void Process(object itemInQueue)
        //{
        //    //When gets the request for  new player from queue
        //    if (itemInQueue is AddPlayerAction) { ProcessAddPlayerAction((AddPlayerAction)itemInQueue); }
        //    if (itemInQueue is SetPositionAction) { ProcessSetPlayerPosition((SetPositionAction)itemInQueue); }
        //    if (itemInQueue is GetAllPlayersPositionAction) { ProcessGetAllPlayersPosition((GetAllPlayersPositionAction)itemInQueue); }
        //    if (itemInQueue is GetObjectStaticPositionsAction) { ProcessGetObjectStaticPositionsAction((GetObjectStaticPositionsAction)itemInQueue); }
        //    if (itemInQueue is GetObjectDynamicPositionAction) { ProcessGetObjectDynamicPositionsAction((GetObjectDynamicPositionAction)itemInQueue); }
        //    if (itemInQueue is GetPlayerScoreAction) { ProcessGetPlayerScoreAction((GetPlayerScoreAction)itemInQueue); }
        //}


        //#region Public Methods
        //public string CreatePlayer(int tileWidth, int tileHeight)
        //{
        //    string playerId = Guid.NewGuid().ToString();
        //    AddPlayer(playerId, tileWidth, tileHeight);
        //    return playerId;
        //}

        //public static void AddPlayer(string playerId, int playerTileW, int playerTileH)
        //{
        //    var addPlayerAction = new AddPlayerAction(playerId, playerTileW, playerTileH);
        //    _queue.Enqueue(addPlayerAction);
        //}

        ///// <summary>
        ///// Sets new player's position by adding SetPositionAction to the queue
        ///// </summary>
        ///// <param name="playerId">Current Player Id</param>
        ///// <param name="newPosition">Requested position</param>
        //public void SetPlayerPosition(string playerId, Vector2 newPosition)
        //{
        //    var setPositionAction = new SetPositionAction(playerId, newPosition);
        //    _queue.Enqueue(setPositionAction);
        //}

        ///// <summary>
        ///// Returns list of players position 
        ///// </summary>
        ///// <param name="playerId"></param>
        ///// <param name="callback"></param>
        //public void GetAllPlayersPosition(Action<List<PlayerData>> callback)
        //{
        //    var getPositionAction = new GetAllPlayersPositionAction(callback);
        //    _queue.Enqueue(getPositionAction);
        //}

        ///// <summary>
        ///// Returns all static objects
        ///// </summary>
        ///// <param name="callback"></param>
        //public void GetObjectStaticPositions(Action<List<Tile>> callback)
        //{
        //    var getObjectPositionsAction = new GetObjectStaticPositionsAction(callback);
        //    _queue.Enqueue(getObjectPositionsAction);
        //}

        ///// <summary>
        ///// Returns all dynamic objects (dimonds, etc)
        ///// </summary>
        ///// <param name="callback"></param>
        //public void GetObjectsDynamicPositions(Action<List<Tile>> callback)
        //{
        //    var getObjectDynamicPositionAction = new GetObjectDynamicPositionAction(callback);
        //    _queue.Enqueue(getObjectDynamicPositionAction);
        //}

        ///// <summary>
        ///// Returns player score by player Id
        ///// </summary>
        ///// <param name="playerId"></param>
        ///// <param name="callback"></param>
        //public void GetPlayerScore(string playerId, Action<int> callback)
        //{
        //    var getPlayerScoreAction = new GetPlayerScoreAction(playerId, callback);
        //    _queue.Enqueue(getPlayerScoreAction);
        //}


        ////public void removePlayer(string id)
        ////{
        ////    _players.Remove(_players.FirstOrDefault(x => x.Id == id));
        ////}

        ////public List<Tile> GetObjectsPositions()
        ////{
        ////    return _tiles;
        ////}
        //#endregion

        //#region Methods that are responsible for processing actions
        //private void ProcessAddPlayerAction(AddPlayerAction action)
        //{
        //    _players.Add(new Player(action.PlayerId, new Vector2(500, 300), PlayerType.Node, action.PlayerTileWidth, action.PlayerTileHeight));
        //}

        //private void ProcessSetPlayerPosition(SetPositionAction action)
        //{
        //    _players.FirstOrDefault(x => x.Id == action.PlayerId).Position = action.NewPlayerPosition;
        //    TileCollision();
        //}

        //private void ProcessGetAllPlayersPosition(GetAllPlayersPositionAction action)
        //{
        //    //var playersPositionList = (from item in players
        //    //                           select item.Position).ToList();
        //    var playersPositionList = _players.Select(i => new PlayerData(i.Id, i.Position)).ToList(); ;
        //    action.Callback(playersPositionList);
        //}

        //private void ProcessGetObjectStaticPositionsAction(GetObjectStaticPositionsAction action)
        //{
        //    //finds all static Objects (walls,doors,floor) in tiles
        //    var staticObjects = (from tile in Tiles
        //                         where tile.TypeofTile == TileType.Wall ||
        //                         tile.TypeofTile == TileType.Door || tile.TypeofTile == TileType.Floor
        //                         select tile).ToList();
        //    action.Callback(staticObjects);
        //}

        //private void ProcessGetObjectDynamicPositionsAction(GetObjectDynamicPositionAction action)
        //{
        //    //finds all Game Objects (diamonds, etc) in tiles
        //    var dynamicObjects = (from tile in Tiles
        //                          where tile.TypeofTile == TileType.GameObject ||
        //                          tile.TypeofTile == TileType.FloorAfterGameObject
        //                          select tile).ToList();
        //    action.Callback(dynamicObjects);
        //}

        //private void ProcessGetPlayerScoreAction(GetPlayerScoreAction action)
        //{
        //    var requestedScore = _players.FirstOrDefault(x => x.Id == action.PlayerId).Score;
        //    action.Callback(requestedScore);
        //}
        //#endregion



        ////public void SetNewDoorPosition(Engine currentEngine, Engine nextEngine, int gateId)
        ////{
        ////    //gets all wall tile in IEnumerateble Colection
        ////    var wallsCollection = from item in Tiles
        ////                          where item.TypeofTile == TileType.Wall
        ////                          select item;

        ////    //makes array from IEnumerateble Colection
        ////    var walls = wallsCollection.ToList();
        ////    //finds random tile from IEnumerateble Colection
        ////    var indexInWalls = new Random().Next(walls.Count());
        ////    //takes this tile from array
        ////    var changedTile = walls[indexInWalls];
        ////    //change tiletype
        ////    changedTile.TypeofTile = TileType.Door;
        ////}

        ///// <summary>
        ///// Removes game object from the list after player intersects with it
        ///// </summary>
        //private void RemoveGameObjectFromMap(Vector2 gameObjectPosition)
        //{
        //    var replacedTile = Tiles.FirstOrDefault(x => x.Position == gameObjectPosition);
        //    replacedTile.TypeofTile = TileType.FloorAfterGameObject;

        //    // finds floor gid number and assign it to replaced tile
        //    var newGidNumber = GidToTileType[TileType.Floor];
        //    replacedTile.Gid = newGidNumber;

        //}

        //private void TileCollision()
        //{
        //    //foreach (var tile in levelMap.Tiles)
        //    for (int i = 0; i < Tiles.Count; i++)
        //    {
        //        for (int j = 0; j < _players.Count; j++)
        //        {
        //            ////checks wall collision
        //            //if ((Math.Abs(Tiles[i].Position.X - _players[j].Position.X) <= Tiles[i].Width &&
        //            //    Math.Abs(Tiles[i].Position.Y - _players[j].Position.Y) <= Tiles[i].Height) &&
        //            //    Tiles[i].TypeofTile == TileType.Wall)
        //            //{
        //            //    //var currentPos = GetPlayerPosition(_players[j].Id);

        //            //    //down
        //            //    //_players[j].Position = new Vector2(_players[j].Position.X, _tiles[i].Position.Y - _players[j].TileHeight);
        //            //    //up
        //            //    //_players[j].Position = new Vector2(_players[j].Position.X, _tiles[i].Position.Y + _tiles[i].Height);
        //            //    //left
        //            //    //_players[j].Position = new Vector2(_tiles[i].Position.X + _tiles[i].Width, _players[j].Position.Y);
        //            //    //RIGHT
        //            //    //_players[j].Position = new Vector2(_tiles[i].Position.X - _players[j].TileWidth, _players[j].Position.Y);
        //            //}

        //            ////checks door collision
        //            //if ((Math.Abs(Tiles[i].Position.X - _players[j].Position.X) <= 20 &&
        //            //    Math.Abs(Tiles[i].Position.Y - _players[j].Position.Y) <= 20) &&
        //            //    Tiles[i].TypeofTile == TileType.Door)
        //            //{
        //            //    //tile.GateTriggered = true;
        //            //}

        //            //checks Game Objects collision
        //            if ((Math.Abs(Tiles[i].Position.X - _players[j].Position.X) <= Tiles[i].Width &&
        //                Math.Abs(Tiles[i].Position.Y - _players[j].Position.Y) <= Tiles[i].Height) &&
        //                Tiles[i].TypeofTile == TileType.GameObject)
        //            {
        //                //Tiles[i].TypeofTile = TileType.Floor;
        //                RemoveGameObjectFromMap(Tiles[i].Position);
        //                _players[j].Score += 1;
        //            }
        //        }
        //    }
        //}


    }
}