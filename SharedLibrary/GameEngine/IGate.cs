﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine
{
    interface IGate
    {
        #region Properties
        Engine CurrentEngine { get; set; }
        Engine NextEngine { get; set; }
        string GateId { get; set; }
        bool GateTriggered { get; }
        byte[] befferRx { get; set; }
        byte[] befferTx { get; set; }
        #endregion
    }
}
