﻿using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class GetPlayerScoreAction
    {
        #region Properties
        public string PlayerId { get; set; }
        public ICallbackPlayerScore Callback { get; set; }
        #endregion

        #region Constructor
        public GetPlayerScoreAction(string playerId, ICallbackPlayerScore callback)
        {
            PlayerId = playerId;
            Callback = callback;
        }
        #endregion
    }
}
