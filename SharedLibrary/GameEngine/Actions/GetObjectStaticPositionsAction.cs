﻿using Microsoft.Xna.Framework;
using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class GetObjectStaticPositionsAction
    {
        #region Properties
        public ICallbackStaticObjects Callback { get; set; }
        #endregion

        #region Constructors
        public GetObjectStaticPositionsAction() { }
        public GetObjectStaticPositionsAction(ICallbackStaticObjects callback)
        {
            Callback = callback;
        }
        #endregion
    }
}
