﻿using Microsoft.Xna.Framework;
using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class GetAllPlayersPositionAction
    {
        #region Properties
        public ICallbackAllPlayersPositions Callback { get; set; }
        #endregion

        #region Constructor
        public GetAllPlayersPositionAction(ICallbackAllPlayersPositions callback)
        {
            Callback = callback;
        }
        #endregion
    }
}
