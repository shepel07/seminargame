﻿using Microsoft.Xna.Framework;
using SharedLibrary.Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class SetPositionAction
    {
        #region Properties
        public string PlayerId { get; set; }
        public CustomVector2 NewPlayerPosition { get; set; }
        #endregion

        #region Constructor
        public SetPositionAction(string playerId, CustomVector2 newPlayerPosition)
        {
            PlayerId = playerId;
            NewPlayerPosition = newPlayerPosition;
        }
        #endregion
    }
}
