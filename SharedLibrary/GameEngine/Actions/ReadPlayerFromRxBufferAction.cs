﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class ReadPlayerFromRxBufferAction
    {
        #region Properties
        public string PlayerId { get; set; }
        #endregion

        #region Constructor
        public ReadPlayerFromRxBufferAction(string playerId)
        {
            PlayerId = playerId;
        }
        #endregion
    }
}
