﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class PutPlayerToRxBufferAction
    {
        #region Properties
        public Player Player { get; set; }
        #endregion

        #region Constructor
        public PutPlayerToRxBufferAction(Player player)
        {
            Player = player;
        }
        #endregion
    }
}
