﻿using SharedLibrary.RemoteEngineObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class GetObjectDynamicPositionAction
    {
        #region Properties
        public ICallbackDynamicObjects Callback { get; set; }
        #endregion

        #region Constructor
        public GetObjectDynamicPositionAction(ICallbackDynamicObjects callback)
        {
            Callback = callback;
        }
        #endregion
    }
}
