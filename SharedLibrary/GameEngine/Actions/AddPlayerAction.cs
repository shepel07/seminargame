﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.GameEngine.Actions
{
    public class AddPlayerAction
    {
        #region Properties
        public string PlayerId { get; set; }
        public int PlayerTileWidth { get; set; }
        public int PlayerTileHeight { get; set; }
        public bool IsSupernode { get; set; }
        #endregion

        #region Constructor
        public AddPlayerAction(string playerId, bool isSupernode, int playerTileWidth, int playerTileHeight)
        {
            PlayerId = playerId;
            IsSupernode = isSupernode;
            PlayerTileWidth = playerTileWidth;
            PlayerTileHeight = playerTileHeight;
        }
        #endregion
    }
}
