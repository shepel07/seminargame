﻿//using Microsoft.Xna.Framework;
//using SharedLibrary.GameEngine.Actions;
//using System;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace SharedLibrary.GameEngine
//{
//    public abstract class BasicEngine
//    {
//        #region Variables
//        /// <summary>
//        /// Holds and process all action from client
//        /// </summary>
//        protected ConcurrentQueue<Object> queue;

//        /// <summary>
//        /// List of players on current map
//        /// </summary>
//        protected List<Player> players;
//        #endregion

//        #region Properties
//        #endregion

//        #region Constructors
//        public BasicEngine()
//        {
//            queue = new ConcurrentQueue<Object>();
//            players = new List<Player>();
//            Start();
//        }
//        #endregion

//        private void Start()
//        {
//            var runthread = new Thread(() =>
//            {
//                // variable, that allows retrieve first element from queue
//                object itemInQueue;
//                while (true)
//                {
//                    queue.TryDequeue(out itemInQueue);
//                    Process(itemInQueue);
//                }
//            });
//            runthread.Start();
//        }

//        protected abstract void Process(object itemInQueue);
//    }
//}
