﻿using SharedLibrary.GameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.RemoteEngineObjects
{
    public static class GameData
    {
        /// <summary>
        /// Stores all information needed to render game. Also this class solves for callbacks
        /// </summary>
        #region Properties
        /// <summary>
        /// Holds all static objects in the map (Walls, Doors, Floor)
        /// </summary>
        public static List<Tile> StaticObjects { get; set; } = new List<Tile>();

        /// <summary>
        /// Holds all dynamic object in the map (diamonds, etc)
        /// </summary>
        public static List<Tile> DynamicObjects { get; set; } = new List<Tile>();

        /// <summary>
        /// Holds player score
        /// </summary>
        public static int PlayerScore { get; set; } = 0;

        /// <summary>
        /// holds all players position and players Id in current map
        /// </summary>
        public static List<PlayerData> PlayersData { get; set; } = new List<PlayerData>();

        /// <summary>
        /// Responsible for changing engine and loading new map static objects (walls...)
        /// </summary>
        public static bool IsNewMap { get; set; } = true;

        /// <summary>
        /// Holds new engine data in string format "127.0.0.1:8888". Allows to change a map in game form, after callback  
        /// </summary>
        public static string NewEngineData { get; set; } = "";

        /// <summary>
        /// registers callback in remote engine
        /// </summary>
        public static ICallbackNewEngineData NewEngineDataMessage { get; set; }

        /// <summary>
        /// Holds player Id, when player changes the map, this Id assigns to variable _playerId inside GF
        /// </summary>
        public static string PlayerId { get; set; } = "";
        #endregion

    }
}
