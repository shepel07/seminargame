﻿using Microsoft.Xna.Framework;
using SharedLibrary.GameEngine;
using SharedLibrary.GameEngine.Actions;
using SharedLibrary.Sharp;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharedLibrary.RemoteEngineObjects
{
    [Serializable]
    public class RemoteEngine : MarshalByRefObject, IEngine
    {

        #region Variables
        private bool _isRunning;
        private List<Gate> _gates;

        /// <summary>
        /// Holds player that in the gate. Player waits until the negatiation between super nodes will over
        /// </summary>
        private List<Player> _txBuffer;

        /// <summary>
        /// Holds players that come from other supernode
        /// </summary>
        private List<Player> _rxBuffer;

        /// <summary>
        /// Holds all known supernodes. Periodically updates its
        /// </summary>
        private List<Supernode> _supernodes;
        /// <summary>
        /// Holds and process all action from client
        /// </summary>
        private static ConcurrentQueue<object> _queue;

        /// <summary>
        /// List of players on current map
        /// </summary>
        private List<Player> _players;

        private Supernode nextSupernode = new Supernode();
        #endregion

        #region Properties
        public string CurrentMapName { get; set; }
        public CustomVector2 InitPositions { get; set; }

        public static ConcurrentQueue<object> Queue
        {
            get { return _queue; }
            set { _queue = value; }
        }


        /// <summary>
        /// holds relationship between Gid from map and custom tileType
        /// </summary>
        public Dictionary<TileType, int> GidToTileType { get; set; }

        public List<Gate> Gates
        {
            get { return _gates; }
            set { _gates = value; }
        }

        public List<Tile> Tiles { get; set; }

        public bool IsRunning
        {
            get { return _isRunning; }
            set { _isRunning = value; }
        }
        #endregion

        #region Constructors
        public RemoteEngine()
        {
        }
        public RemoteEngine(List<Supernode> supernodes, List<Tile> tiles, string mapName, Dictionary<TileType, int> dict, int port)
        {
            _supernodes = supernodes;
            Tiles = tiles;
            CurrentMapName = mapName;
            GidToTileType = dict;
            _gates = new List<Gate>();
            _queue = new ConcurrentQueue<object>();
            _players = new List<Player>();
            _txBuffer = new List<Player>();
            _rxBuffer = new List<Player>();
            Start();
        }
        #endregion

        private void Start()
        {
            var runthread = new Thread(() =>
            {
                // variable, that allows retrieve first element from queue
                object itemInQueue;
                while (true)
                {
                    _queue.TryDequeue(out itemInQueue);
                    Process(itemInQueue);
                }
            });
            runthread.Start();
        }

        /// <summary>
        /// Process messages from the queue
        /// </summary>
        /// <param name="itemInQueue"></param>
        private void Process(object itemInQueue)
        {
            //When gets the request for  new player from queue
            if (itemInQueue is AddPlayerAction) { ProcessAddPlayerAction((AddPlayerAction)itemInQueue); }
            if (itemInQueue is SetPositionAction) { ProcessSetPlayerPosition((SetPositionAction)itemInQueue); }
            if (itemInQueue is GetAllPlayersPositionAction) { ProcessGetAllPlayersPosition((GetAllPlayersPositionAction)itemInQueue); }
            if (itemInQueue is GetObjectStaticPositionsAction) { ProcessGetObjectStaticPositionsAction((GetObjectStaticPositionsAction)itemInQueue); }
            if (itemInQueue is GetObjectDynamicPositionAction) { ProcessGetObjectDynamicPositionsAction((GetObjectDynamicPositionAction)itemInQueue); }
            if (itemInQueue is GetPlayerScoreAction) { ProcessGetPlayerScoreAction((GetPlayerScoreAction)itemInQueue); }
            if (itemInQueue is PutPlayerToTxBufferAction) { ProcessPutPlayerToTxBufferAction((PutPlayerToTxBufferAction)itemInQueue); }
            if (itemInQueue is PutPlayerToRxBufferAction) { ProcessPutPlayerToRxBufferAction((PutPlayerToRxBufferAction)itemInQueue); }
            if (itemInQueue is ReadPlayerFromRxBufferAction) { ProcessReadPlayerFromRxBufferAction((ReadPlayerFromRxBufferAction)itemInQueue); }
        }

        /// <summary>
        /// Register callback. Allows to send new engine data, when SN moves from its own map to oter map 
        /// </summary>
        /// <param name="mes"></param>
        public void RegisterCallback(ICallbackNewEngineData mes)
        {
            // _newEngineDataMessage = mes;
            GameData.NewEngineDataMessage = mes;
             
        }

        #region Public Methods
        public string CreatePlayer(int tileWidth, int tileHeight, bool isSupernode)
        {
            string playerId = Guid.NewGuid().ToString();
            AddPlayer(playerId, isSupernode, tileWidth, tileHeight);
            return playerId;
        }

        public static void AddPlayer(string playerId, bool isSupernode, int playerTileW, int playerTileH)
        {
            var addPlayerAction = new AddPlayerAction(playerId, isSupernode, playerTileW, playerTileH);
            _queue.Enqueue(addPlayerAction);
        }

        /// <summary>
        /// Sets new player's position by adding SetPositionAction to the queue
        /// </summary>
        /// <param name="playerId">Current Player Id</param>
        /// <param name="newPosition">Requested position</param>
        public void SetPlayerPosition(string playerId, CustomVector2 newPosition)
        {
            var setPositionAction = new SetPositionAction(playerId, newPosition);
            _queue.Enqueue(setPositionAction);
        }

        /// <summary>
        /// Returns list of players position 
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="callback"></param>
        public void GetAllPlayersPosition(ICallbackAllPlayersPositions callback)
        {
            var getPositionAction = new GetAllPlayersPositionAction(callback);
            _queue.Enqueue(getPositionAction);
        }

        /// <summary>
        /// Returns all static objects
        /// </summary>
        /// <param name="callback"></param>
        public void GetObjectStaticPositions(ICallbackStaticObjects callback)
        {
            var getObjectPositionsAction = new GetObjectStaticPositionsAction(callback);
            _queue.Enqueue(getObjectPositionsAction);
        }

        /// <summary>
        /// Returns all dynamic objects (dimonds, etc)
        /// </summary>
        /// <param name="callback"></param>
        public void GetObjectsDynamicPositions(ICallbackDynamicObjects callback)
        {
            var getObjectDynamicPositionAction = new GetObjectDynamicPositionAction(callback);
            _queue.Enqueue(getObjectDynamicPositionAction);
        }

        /// <summary>
        /// Returns player score by player Id
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="callback"></param>
        public void GetPlayerScore(string playerId, ICallbackPlayerScore callback)
        {
            var getPlayerScoreAction = new GetPlayerScoreAction(playerId, callback);
            _queue.Enqueue(getPlayerScoreAction);
        }

        /// <summary>
        /// This method allows to put player on this map from remote side
        /// </summary>
        /// <param name="player"></param>
        public void PutPlayerToRxBuffer(Player player)
        {
            var putPlayerToRxBufferAction = new PutPlayerToRxBufferAction(player);
            _queue.Enqueue(putPlayerToRxBufferAction);
        }

        public void removePlayer(string playerId)
        {
            _players.Remove(_players.FirstOrDefault(x => x.Id == playerId));
        }

        #endregion

        #region Methods that are responsible for processing actions
        private void ProcessAddPlayerAction(AddPlayerAction action)
        {
            if (action.IsSupernode == true)
                _players.Add(new Player(action.PlayerId, new CustomVector2(500, 300), PlayerType.Supernode, action.PlayerTileWidth, action.PlayerTileHeight));
            else
                _players.Add(new Player(action.PlayerId, new CustomVector2(500, 300), PlayerType.Node, action.PlayerTileWidth, action.PlayerTileHeight));

        }

        private void ProcessSetPlayerPosition(SetPositionAction action)
        {
            var player = _players.FirstOrDefault(x => x.Id == action.PlayerId);
            if (player != null)
            {
                player.Position = action.NewPlayerPosition;
                TileCollision();
            }
        }

        private void ProcessGetAllPlayersPosition(GetAllPlayersPositionAction action)
        {
            var playersPositionList = _players.Select(i => new PlayerData(i.Id, i.Position)).ToList(); ;
            action.Callback.GetAllPlayersPositions(playersPositionList);
        }

        private void ProcessGetObjectStaticPositionsAction(GetObjectStaticPositionsAction action)
        {
            //finds all static Objects (walls,doors,floor) in tiles
            var statObjs = (from tile in Tiles
                            where tile.TypeofTile == TileType.Wall ||
                            tile.TypeofTile == TileType.Door || tile.TypeofTile == TileType.Floor
                            select tile).ToList();
            action.Callback.GetStaticObjects(statObjs);
        }

        private void ProcessGetObjectDynamicPositionsAction(GetObjectDynamicPositionAction action)
        {
            //finds all Game Objects (diamonds, etc) in tiles
            var dynamicObjects = (from tile in Tiles
                                  where tile.TypeofTile == TileType.GameObject ||
                                  tile.TypeofTile == TileType.FloorAfterGameObject
                                  select tile).ToList();
            action.Callback.GetDynamicObjects(dynamicObjects);
        }

        private void ProcessGetPlayerScoreAction(GetPlayerScoreAction action)
        {
            var player = _players.FirstOrDefault(x => x.Id == action.PlayerId);
            if (player != null)
            {
                var requestedScore = player.Score;
                action.Callback.GetPlayerScore(requestedScore);
            }
        }

        private void ProcessPutPlayerToTxBufferAction(PutPlayerToTxBufferAction action)
        {
            PutPlayerToTxBuffer(action.PlayerId);
        }

        private void ProcessPutPlayerToRxBufferAction(PutPlayerToRxBufferAction action)
        {
            _rxBuffer.Add(action.Player);
            var readPlayerFromRxBufferAction = new ReadPlayerFromRxBufferAction(action.Player.Id);
            _queue.Enqueue(readPlayerFromRxBufferAction);
        }

        private void ProcessReadPlayerFromRxBufferAction(ReadPlayerFromRxBufferAction action)
        {
            var id = action.PlayerId;
            var player = _rxBuffer.FirstOrDefault(x => x.Id == id);
            _rxBuffer.Remove(player);
            player.Position = new CustomVector2(550, 500);
            //GameData.PlayerId = id;
            _players.Add(player);
        }
        #endregion



        //public void SetNewDoorPosition(Engine currentEngine, Engine nextEngine, int gateId)
        //{
        //    //gets all wall tile in IEnumerateble Colection
        //    var wallsCollection = from item in Tiles
        //                          where item.TypeofTile == TileType.Wall
        //                          select item;

        //    //makes array from IEnumerateble Colection
        //    var walls = wallsCollection.ToList();
        //    //finds random tile from IEnumerateble Colection
        //    var indexInWalls = new Random().Next(walls.Count());
        //    //takes this tile from array
        //    var changedTile = walls[indexInWalls];
        //    //change tiletype
        //    changedTile.TypeofTile = TileType.Door;
        //}

        /// <summary>
        /// Removes game object from the list after player intersects with it
        /// </summary>
        private void RemoveGameObjectFromMap(CustomVector2 gameObjectPosition)
        {
            var replacedTile = Tiles.FirstOrDefault(x => x.Position == gameObjectPosition);
            replacedTile.TypeofTile = TileType.FloorAfterGameObject;

            // finds floor gid number and assign it to replaced tile
            var newGidNumber = GidToTileType[TileType.Floor];
            replacedTile.Gid = newGidNumber;

        }

        /// <summary>
        /// Removes player from player list and puts it temporary to TxBuffer
        /// </summary>
        /// <param name="playerId"></param>
        private void PutPlayerToTxBuffer(string playerId)
        {
            //player, that whants to change map
            var player = _players.FirstOrDefault(x => x.Id == playerId);
            removePlayer(playerId);
            _txBuffer.Add(player);

            //looks for next supernode
            nextSupernode = _supernodes.FirstOrDefault(x => x.Map == "m1");

            //takes remote object of next supernode
            #region RMI stuff
            var provider = new BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = TypeFilterLevel.Full;
            var dictionary = new System.Collections.Hashtable();

            // variable port needs to create tcp channel. it is completely different variable than _post. 
            Random rnd = new Random();
            var port = rnd.Next(47001, 62000);
            dictionary["port"] = port;
            dictionary["name"] = $"tcp{port}";
            var clientRemotingChannel = new TcpChannel(dictionary, null, provider);
            ChannelServices.RegisterChannel(clientRemotingChannel, false);
            #endregion

            // connection string vith SN ip and port. 
            string fullUrl = $"tcp://{nextSupernode.Ip}:{nextSupernode.Port}/remoteEngine";
            var remoteEngineNextSupernode = (RemoteEngine)Activator.GetObject(typeof(RemoteEngine), fullUrl);

            //puts player to remote RxBuffer
            remoteEngineNextSupernode.PutPlayerToRxBuffer(player);

            GameData.NewEngineDataMessage.SendNewEngineData(nextSupernode.Ip, nextSupernode.Port);
            //_newEngineDataMessage.SendNewEngineData(nextSupernode.Ip, nextSupernode.Port);
        }

        private void TileCollision()
        {
            //foreach (var tile in levelMap.Tiles)
            for (int i = 0; i < Tiles.Count; i++)
            {
                for (int j = 0; j < _players.Count; j++)
                {
                    ////checks wall collision
                    //if ((Math.Abs(Tiles[i].Position.X - _players[j].Position.X) <= Tiles[i].Width &&
                    //    Math.Abs(Tiles[i].Position.Y - _players[j].Position.Y) <= Tiles[i].Height) &&
                    //    Tiles[i].TypeofTile == TileType.Wall)
                    //{
                    //    //var currentPos = GetPlayerPosition(_players[j].Id);

                    //    //down
                    //    //_players[j].Position = new Vector2(_players[j].Position.X, _tiles[i].Position.Y - _players[j].TileHeight);
                    //    //up
                    //    //_players[j].Position = new Vector2(_players[j].Position.X, _tiles[i].Position.Y + _tiles[i].Height);
                    //    //left
                    //    //_players[j].Position = new Vector2(_tiles[i].Position.X + _tiles[i].Width, _players[j].Position.Y);
                    //    //RIGHT
                    //    //_players[j].Position = new Vector2(_tiles[i].Position.X - _players[j].TileWidth, _players[j].Position.Y);
                    //}

                    //checks door collision
                    if ((Math.Abs(Tiles[i].Position.X - _players[j].Position.X) <= 20 &&
                        Math.Abs(Tiles[i].Position.Y - _players[j].Position.Y) <= 20) &&
                        Tiles[i].TypeofTile == TileType.Door)
                    {
                        var putPlayerToTxBufferAction = new PutPlayerToTxBufferAction(_players[j].Id);
                        _queue.Enqueue(putPlayerToTxBufferAction);
                    }

                    //checks Game Objects collision
                    if ((Math.Abs(Tiles[i].Position.X - _players[j].Position.X) <= (Tiles[i].Width - 10) &&
                        Math.Abs(Tiles[i].Position.Y - _players[j].Position.Y) <= (Tiles[i].Height - 10)) &&
                        Tiles[i].TypeofTile == TileType.GameObject)
                    {
                        RemoveGameObjectFromMap(Tiles[i].Position);
                        _players[j].Score += 1;
                    }
                }
            }
        }

    }
}
