﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.RemoteEngineObjects
{
    public interface ICallbackPlayerScore
    {
        void GetPlayerScore(int score);
    }
}
