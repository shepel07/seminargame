﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.Utilities
{
    public interface IBytePacket
    {
        byte[] Pack();
        byte[] Pack(out int length);
    }
}
