﻿using SharedLibrary.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.Networking.Sockets
{
    [Serializable]
    public class RequestConnectionPacket : BytePacket<RequestConnectionPacket>
    {
        #region Properties
        public string PlayerName { get; set; }

        /// <summary>
        /// if player will be SN, this port will be used for RMI
        /// </summary>
        public int Port { get; set; }

        #endregion

        #region Constructors
        public RequestConnectionPacket() { }

        public RequestConnectionPacket(string name, int port)
        {
            PlayerName = name;
            Port = port;
        }
        #endregion
    }
}
