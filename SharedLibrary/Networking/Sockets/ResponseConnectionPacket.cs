﻿using SharedLibrary.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary.Networking.Sockets
{
    [Serializable]
    public class ResponseConnectionPacket : BytePacket<ResponseConnectionPacket>
    {
        #region Properties
        public List<Supernode> Supernodes { get; set; }
        public bool IsSupernode { get; set; }
        public string MapName { get; set; }
        #endregion

        #region Constructors
        public ResponseConnectionPacket(List<Supernode> snList, bool isSn, string mapName)
        {
            Supernodes = snList;
            IsSupernode = isSn;
            MapName = mapName;
        }
        #endregion
    }
}
