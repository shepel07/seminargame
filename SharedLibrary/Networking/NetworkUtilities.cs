﻿using SharedLibrary.Networking.RMI;
using SharedLibrary.Networking.Sockets;
using SharedLibrary.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SharedLibrary.Networking
{
    public class NetworkUtilities
    {
        #region Variables
        private static Socket _clientSocket;
        private const int port = 7777;
        private const int bufferSize = 1024;
        private static byte[] _buffer = new byte[bufferSize];
        private static IPAddress ipAddr = IPAddress.Parse("127.0.0.1");
        //private static IPAddress ipAddr = IPAddress.Parse("10.0.5.229");
        private static IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
        #endregion

        /// <summary>
        /// Initiates connection to server and initial negotiation between client and server
        /// </summary>
        public static ResponseConnectionPacket SendConnectionRequestToServer(int port, string playerName)
        {
            LoopConnectToServer(ipEndPoint);
            var response = SendRequestToServer(ipEndPoint, playerName, port);
            return response;
        }

        /// <summary>
        /// Receives info from serveer about available game maps
        /// </summary>
        private static ResponseConnectionPacket SendRequestToServer(IPEndPoint socketSourse, string playerName, int port)
        {
            //creates request packet and converts to bytes. Puts name to packet. Sends socket to server
            var requestPacket = new RequestConnectionPacket(playerName, port);
            var requestBytes = requestPacket.Pack();
            _clientSocket.Send(requestBytes);

            //receives response from server. Parses bytes to Response object. Extracts info from response object
            byte[] receivedBuffer = new byte[bufferSize];
            int received = _clientSocket.Receive(receivedBuffer);
            byte[] data = new byte[received];
            Array.Copy(receivedBuffer, data, received);
            var responseConnectionData = BytePacket<ResponseConnectionPacket>.UnPack(receivedBuffer);
            CloseSocket(_clientSocket);
            return responseConnectionData;
        }

        public static void SendRequestForAllSupernodes()
        {
            LoopConnectToServer(ipEndPoint);
        }


        /// <summary>
        /// Close socket properly
        /// </summary>
        /// <param name="currentSocket"></param>
        private static void CloseSocket(Socket currentSocket)
        {
            currentSocket.Shutdown(SocketShutdown.Both);
            currentSocket.Close();
        }

        /// <summary>
        /// Creates socket for server connection and sets connection
        /// </summary>
        /// <param name="distinationSourse">Server IP address and port</param>
        /// <param name="form">current WinForm. Display a state of connection</param>
        private static void LoopConnectToServer(IPEndPoint distinationSourse)
        {
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            int i = 3;
            while (!_clientSocket.Connected && i != 0)
            {
                try
                {
                    _clientSocket.Connect(distinationSourse);
                }
                catch (SocketException)
                {
                }
                i--;
            }
        }


    }
}
